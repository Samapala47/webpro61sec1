<?php
$txt = "Hello world";
$number = 10;

echo $txt ;
echo "<br>";
echo $number;
echo "<h4>This is a simple heading.</h4>";
echo "<h4 style='color:red'>This is heading with style.</h4>";

$iint = 123;
var_dump($iint);
echo "<br>";

$sstring = 'Hi bro';
echo $sstring;
echo"<br>";

$color = array("Red","Green","Blue");
var_dump($color);
echo "<br>";

$color_codes = array(
	"Red" => "#ff0000",
	"Green" => "#00f00",
	"Blue" => "#0000ff"
);
var_dump($color_codes);
echo "<br>";

class Greeting{
	public $str = "Hi mom";

	function show_greeting(){
		return $this -> str;
	}

}

$message = new Greeting;
var_dump($message);
echo "<br>";
echo $message -> show_greeting();
echo "<br>";

?>







